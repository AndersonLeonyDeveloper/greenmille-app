package br.com.greenmilleapp.events.request

import br.com.greenmilleapp.events.base.BaseEvent

class SucessEvent: BaseEvent<String> {

    constructor(msgSucess:String) : super(msgSucess)

}