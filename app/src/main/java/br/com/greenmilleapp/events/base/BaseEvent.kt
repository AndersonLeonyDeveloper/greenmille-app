package br.com.greenmilleapp.events.base

open abstract class BaseEvent <T> {

    private var data: T? = null

    constructor(data: T?){
        this.data = data
    }

    fun getData() = this.data

}