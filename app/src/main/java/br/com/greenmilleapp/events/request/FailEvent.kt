package br.com.greenmilleapp.events.request

import br.com.greenmilleapp.events.base.BaseEvent

class FailEvent : BaseEvent<String>{

    constructor(msgFail:String) : super(msgFail)

}