package br.com.greenmilleapp.events

class MessageEvent(messageEvent: String) {

    var message: String = ""

    init {
        this.message = messageEvent
    }

}