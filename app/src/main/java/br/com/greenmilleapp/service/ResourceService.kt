package br.com.greenmilleapp.service

import br.com.greenmilleapp.events.request.FailEvent
import br.com.greenmilleapp.events.request.SucessEvent
import br.com.greenmilleapp.models.Resource
import br.com.greenmilleapp.models.ResourceDeserialized
import io.realm.Realm
import io.realm.Sort
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


object ResourceService {

    private var api: API = ServiceBuilder.buildService(API::class.java)

    fun requestResourceList() {

        api.getAllResources().enqueue(object : Callback<MutableList<ResourceDeserialized>> {

            override fun onResponse(call: Call<MutableList<ResourceDeserialized>>, response: Response<MutableList<ResourceDeserialized>>) {

                if (response.code()==200) {

                    val resourceList = response.body()!!

                    if(getResources().isEmpty()) {

                        for (i in resourceList.indices) {
                            addResource(resourceList[i].mResource)
                        }

                    }

                    EventBus.getDefault().post(SucessEvent(response.message()+""))

                }

            }

            override fun onFailure(call: Call<MutableList<ResourceDeserialized>?>?, t: Throwable) {
                EventBus.getDefault().post(FailEvent(msgFail = t.message.toString()))
            }

        })
    }

    fun addResource(resource: Resource) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.beginTransaction()

        if(resource.id == 0){
            val max: Number? = realm.where(Resource::class.java).max("id")
            var nextId: Int

            nextId = 1

            max?.let {
                //there is some resource
                nextId = max.toInt() + 1
            }

            resource.id = nextId
        }

        realm.copyToRealmOrUpdate(resource)
        realm.commitTransaction()

    }

    fun getResources() : MutableList<Resource>{
        var realm: Realm = Realm.getDefaultInstance()

        return realm.copyFromRealm(realm.where(Resource::class.java)
            .sort("id", Sort.ASCENDING)
            .findAll() ) ?: mutableListOf()

    }

    fun getResourcesWithPagination(page: Int, quantity: Int): MutableList<Resource> {
        var realm = Realm.getDefaultInstance()
        return  getSubListForPagination(realm.copyFromRealm(realm.where(Resource::class.java)
            .findAll()), page, quantity)
    }
    fun getSubListForPagination(list:MutableList<Resource>, page: Int, quantity: Int): MutableList<Resource>{

        var fromIndex = page
        var toIndex = page + quantity
        if(toIndex > list.size){
            toIndex = list.size - 1
        }

        return list.subList(fromIndex, toIndex)

    }

}