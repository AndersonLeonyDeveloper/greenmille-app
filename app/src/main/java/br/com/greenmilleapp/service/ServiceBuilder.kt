package br.com.greenmilleapp.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ServiceBuilder {
    private const val URL = "http://portal.greenmilesoftware.com/"

    fun <T> buildService(serviceType: Class<T>): T {

        var loggingInterceptor = HttpLoggingInterceptor()
        var okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

        okHttpClientBuilder.addInterceptor(loggingInterceptor)
        okHttpClientBuilder.connectTimeout(1000, TimeUnit.SECONDS)
        okHttpClientBuilder.readTimeout(1000, TimeUnit.SECONDS)
        okHttpClientBuilder.writeTimeout(1000, TimeUnit.SECONDS)

        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClientBuilder.build())

        val retrofit = retrofitBuilder.build()


        return retrofit.create(serviceType)

    }

}