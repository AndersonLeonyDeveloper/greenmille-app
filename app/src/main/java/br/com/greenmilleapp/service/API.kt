package br.com.greenmilleapp.service

import br.com.greenmilleapp.models.ResourceDeserialized
import retrofit2.Call
import retrofit2.http.GET

interface API {

    @GET("get_resources_since")
    fun getAllResources(): Call<MutableList<ResourceDeserialized>>
}