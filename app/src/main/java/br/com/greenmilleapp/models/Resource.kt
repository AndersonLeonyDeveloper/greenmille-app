package br.com.greenmilleapp.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.io.Serializable

@RealmClass
open class Resource(
    @PrimaryKey
    var id: Int? = 0,

    @SerializedName("created_at")
    var createdAt: String? = null,

    @SerializedName("updated_at")
    var updatedAt: String? = null,

    @SerializedName("resource_id")
    var resourceId: String? = null,

    @SerializedName("module_id")
    var moduleId: String? = null,

    @SerializedName("value")
    var value: String? = null,

    @SerializedName("language_id")
    var languageId: String? = null

) : RealmObject(){

}