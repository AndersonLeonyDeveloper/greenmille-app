package br.com.greenmilleapp.models

import com.google.gson.annotations.SerializedName

open class ResourceDeserialized (

    @SerializedName("resource")
    var mResource: Resource

){

}