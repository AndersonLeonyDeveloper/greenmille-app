package br.com.greenmilleapp.adapters.recycleViewAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.greenmilleapp.R
import br.com.greenmilleapp.models.Resource
import br.com.greenmilleapp.service.ResourceService
import br.com.greenmilleapp.views.items.ResourceItem

class ResourceAdapter(
    private var resourceList: MutableList<Resource> = ResourceService.getResources(),
    private var resourceOldList: MutableList<Resource> = mutableListOf(),
    private var context: Context
) : RecyclerView.Adapter<ResourceItem>() {

    init {
        this.resourceOldList.addAll(this.resourceList)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ResourceItem {
        val v: View = LayoutInflater.from(this.context).inflate(R.layout.item_resource, viewGroup, false)

        return ResourceItem(v)
    }

    override fun getItemCount(): Int {
        return resourceList.size
    }

    override fun onBindViewHolder(holder: ResourceItem, position: Int) {
        holder.bind(resourceList[position])
    }

    fun withPagination(page: Int, quantity: Int): MutableList<Resource>{
        this.resourceList = ResourceService.getResourcesWithPagination(page, quantity)

        notifyDataSetChanged()

        return this.resourceList
    }

    fun updateAdapterAfterScroll(page: Int, quantity: Int){
        this.resourceList.addAll(ResourceService.getResourcesWithPagination(page, quantity))
        this.resourceOldList = this.resourceList

        notifyDataSetChanged()
    }

    fun filterByValue(value: String){
        var filteredList: MutableList<Resource> = mutableListOf()

        this.resourceOldList.forEach { resource ->
            if("${resource.value}".equals(value)){
                filteredList.add(resource)
            }
        }

        if(filteredList.isEmpty()){

            insertList(resourceOldList)
        }else{
            insertList(filteredList)
        }
    }

    fun filterByLanguageId(languageId: String){
        var filteredList: MutableList<Resource> = mutableListOf()

        this.resourceOldList.forEach { resource ->
            if("${resource.languageId}".equals(languageId)){
                filteredList.add(resource)
            }
        }

        if(filteredList.isEmpty()){

            insertList(resourceOldList)
        }else{
            insertList(filteredList)
        }
    }

    fun filterByModuleId(moduleId: String){
        var filteredList: MutableList<Resource> = mutableListOf()

        this.resourceOldList.forEach { resource ->
            if("${resource.moduleId}".equals(moduleId)){
                filteredList.add(resource)
            }
        }

        if(filteredList.isEmpty()){

            insertList(resourceOldList)
        }else{
            insertList(filteredList)
        }
    }

    fun insertList(list: MutableList<Resource>){

        this.resourceList = list

        notifyDataSetChanged()

    }


}