package br.com.greenmilleapp.application

import android.app.Application
import io.realm.Realm

class GreenMilleApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
    }
}