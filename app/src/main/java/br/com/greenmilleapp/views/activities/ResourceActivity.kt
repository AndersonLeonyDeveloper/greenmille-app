package br.com.greenmilleapp.views.activities

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.AbsListView
import android.widget.CheckBox
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.greenmilleapp.R
import br.com.greenmilleapp.adapters.recycleViewAdapters.ResourceAdapter
import br.com.greenmilleapp.events.request.FailEvent
import br.com.greenmilleapp.events.request.SucessEvent
import br.com.greenmilleapp.service.ResourceService
import br.com.greenmilleapp.views.activities.baseActivity.BaseActivity
import kotlinx.android.synthetic.main.activity_resource.*
import org.greenrobot.eventbus.Subscribe

class ResourceActivity : BaseActivity() {

    private lateinit var resourceAdapter: ResourceAdapter

    var currentVisibleItems: Int = 0
    var totalItems: Int = 0
    var scrollOutItems: Int = 0

    var isScrolling: Boolean = false

    lateinit var layoutManager: LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resource)

        enableFields()

        ResourceService.requestResourceList()

        resourceAdapter = ResourceAdapter(context = this)

        if(!ResourceService.getResources().isEmpty()){
            resourceAdapter.withPagination(0, 10)
        }else{
            progressBar.visibility = View.VISIBLE
        }

        layoutManager = LinearLayoutManager(this)

        rv_resource.adapter = resourceAdapter

        rv_resource.layoutManager = layoutManager

        rv_resource.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true

                }

            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                currentVisibleItems = layoutManager.childCount

                totalItems = layoutManager.itemCount

                scrollOutItems = layoutManager.findFirstVisibleItemPosition()

                if (dy > 0) {
                    if (isScrolling && (currentVisibleItems + scrollOutItems == totalItems) && ifSomeCheckBoxAreActivated()) {

                        isScrolling = false

                        fetchData(totalItems)
                    }
                }
            }
        })

        edtFilterValue.doAfterTextChanged {

            resourceAdapter.filterByValue(it.toString())

        }

        edtFilterLanguageId.doAfterTextChanged {
            resourceAdapter.filterByLanguageId(it.toString())
        }

        edtFilterModuleId.doAfterTextChanged {
            resourceAdapter.filterByModuleId(it.toString())
        }

    }

    private fun fetchData(totalItems: Int) {
        progressBar.visibility = View.VISIBLE
        Handler().postDelayed(Runnable {

            resourceAdapter.updateAdapterAfterScroll(page = resourceAdapter.itemCount - 1, quantity = 10)

            progressBar.visibility = View.GONE

        }, 5000)

    }

    @Subscribe
    fun onEvent(dado: SucessEvent) {

        progressBar.visibility = View.GONE

        resourceAdapter.withPagination(0, 10)

        rv_resource.adapter = resourceAdapter

        rv_resource.layoutManager = layoutManager


    }

    @Subscribe
    fun onEvent(dado: FailEvent) {

        progressBar.visibility = View.GONE

    }

    fun enableFields(){
        edtFilterValue.isEnabled = false
        edtFilterLanguageId.isEnabled = false
        edtFilterModuleId.isEnabled = false
    }

    fun ifSomeCheckBoxAreActivated():Boolean{

        if(checkBoxValue.isChecked == false && checkBoxLanguageId.isChecked == false && checkBoxModuleId.isChecked == false){
            return true
        }else{
            return false
        }
    }

    fun onCheckboxClicked(view: View) {
        if (view is CheckBox) {
            val checked: Boolean = view.isChecked

            when (view.id) {
                R.id.checkBoxValue -> {
                    if (checked) {
                        edtFilterValue.isEnabled = true
                    } else {
                        edtFilterValue.isEnabled = false
                        edtFilterValue.setText("")

                    }
                }
                R.id.checkBoxLanguageId -> {
                    if (checked) {
                        edtFilterLanguageId.isEnabled = true
                    } else {
                        edtFilterLanguageId.isEnabled = false
                        edtFilterLanguageId.setText("")

                    }
                }
                R.id.checkBoxModuleId -> {
                    if (checked) {
                        edtFilterModuleId.isEnabled = true
                    } else {
                        edtFilterModuleId.isEnabled = false
                        edtFilterModuleId.setText("")

                    }
                }

            }
        }
    }
}
