package br.com.greenmilleapp.views.items

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.greenmilleapp.models.Resource
import kotlinx.android.synthetic.main.item_resource.view.*

class ResourceItem(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var resourceId = itemView.tv_resource_id
    var updatedAt = itemView.tv_updated_at
    var value = itemView.tv_value

    fun bind(resource: Resource){

        resourceId.text = resource.resourceId
        updatedAt.text = resource.updatedAt
        value.text = resource.value

    }
}