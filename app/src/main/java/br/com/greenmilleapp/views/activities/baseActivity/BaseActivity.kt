package br.com.greenmilleapp.views.activities.baseActivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.greenmilleapp.events.MessageEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @Subscribe
    fun onMessageEvent(event: MessageEvent){

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
}